# Seja bem vindo ao Debian

O mundo GNU é fascinante e nos oferece um amplo leque de possíveis direções a seguir. Ocorre que, enquanto todas estas possibilidades seja o que torna a jornada tão interessante, sabemos que este é exatamente o que torna o início tão difícil.

Pensando nisso, criamos este pequeno guia para te ajudar com os primeiros passos.

Esta é a sua página inicial, nela você encontrará apenas o essencial para não se sentir completamente perdido em seu sistema e nas conversas com os membros desta viva comunidade. Ao final, sugerimos algumas fontes de pesquisa, mas tentamos reunir as dúvidas e problemas mais comuns [aqui](https://gitlab.com/vbra/debian-bemvindo/-/wikis/home).

# O que é linux, GNU e Debian?

No cotidiano com a comunidade do Debian, você vai acabar percebendo o constante uso dos termos "linux" e "GNU", mas é bom que não se confunda: eles não são sinônimos.

O *linux* é o kernel do **seu** sistema operacional, por enquanto você não precisa saber os detalhes sobre o que é um kernel, basta entender que ele é uma parte do todo. Assim, como um motor é parte de um carro, o kernel é parte do **seu** sistema operacional.

O *GNU* é uma coleção de muitos programas que tornam o seu computador realmente útil.

Já o *Debian* é uma distribuição GNU+Linux, a qual segue uma filosofia e metologia muito própria e que visa disponibilizar um conjunto de programas e documentação que respeita a liberdade dos indivíduos, ao mesmo tempo que luta pela construção de um sistema estável e verdadeiramente livre.

# O visual da minha instalação é diferente das que já vi, o que está acontecendo?

Quando você instala o Debian, normalmente, seleciona um *ambiente gráfico*. Cada ambiente destes possui aparência, funcionalidades e programas padrões diferentes.

Não se preocupe, você pode mudar o ambiente, as funcionalidades e os programas sempre que quiser.

Para conhecer um pouco mais destes ambientes [clique aqui](https://debian-handbook.info/browse/pt-BR/stable/sect.graphical-desktops.html).

# Onde posso encontrar informações oficiais?

Todos os que procuram informações sobre eventos atuais e progresso do desenvolvimento na comunidade Debian podem estar acessar as [Notícias do Projeto Debian](https://www.debian.org/News/weekly/), o blog oficial do Debian [Bits do Debian](https://bits.debian.org/) e as [Micronotícias do Debian](https://micronews.debian.org/). Se esta é sua preferência, sugiro pesquisar um pouco sobre leitores RSS, existem ótimos programas livres por aí.

Outra opção é se cadastrar nas listas de de discussão por e-mail [debian-announce](https://lists.debian.org/debian-announce/) e [debian-news](https://lists.debian.org/debian-news/). 

Por fim, se sua preferência for o bom e velho bate-papo, seu caminho oficial é o IRC, usando o OFTC e conectando a irc.debian.org

"Mas e se eu não gostar destas opções? Ouvi falar que existem grupos, por exemplo, no Telegram..."

Sim, é verdade que existem muitos grupos por aí, e eles são excelentes, mas é importante saber que não se tratam de veículos de comunicação oficial, mas de canais criados por integrantes da comunidade que podem, ou não, seguir as diretrizes gerais do projeto Debian.

# Onde posso encontrar ajuda?

Para os recém chegados, os integrantes da comunidade costumam oferecer uma ótima ajuda voluntária em vários canais de comunicação, nós sugerimos os grupos [Debian Brasil](https://t.me/debianbrasil) e o [Curso GNU (kretcheu)](https://t.me/cursognu) no Telegram.

Mas lembre-se: estes canais não oferecem um suporte ao usuário, mas, sim, uma ajuda voluntária. Então é sempre aconselhável ser educado e ter bom senso 😉

# Como posso obter ajudar de forma eficiente?

Este é um ponto interessante, existem várias formas de se pedir ajuda e algumas são melhores do que outras.

Minha susgestão: assista este vídeo e tenha uma vida mais fácil: 

[![Assistir](http://img.youtube.com/vi/D5fipnSQ9P0/0.jpg)](http://www.youtube.com/watch?v=D5fipnSQ9P0 "")

# Como posso instalar programas?

Já que você está chegando ao Debian agora, minha sugestão pode parecer contraintuitiva, mas acredite: seu dia a dia será mais fácil se você der uma chance ao terminal (mas cuidado, é viciante 😀)

Assim sendo, para instalar um programa tudo o que você precisa fazer é:
1. Abrir um terminal
2. Pesquisar o nome do programa que você deseja com `apt search nome-do-programa`
3. Instalar o programa com `sudo apt install nome-do-programa`

"E o que mais?"

Nada... É isso, o programa já está instalado e usando toda segurança que o Debian te oferece.

# Não encontro os programas que eu usava, o que posso fazer?

Neste caso o melhor é procurar alternativas.

A lógica é simples: cada sistema operacional tende a possuir programas diferentes para funcionalidades parecidas. 

Nossa sugestão é: não se apegue a um programa específico, mas sim ao problema que você quer resolver com ele. É muito provável que existam excelentes alternativas que você ainda não conhece.

# Quero me aprofundar mais, quais os próximos passos?

Existe um mundo de opções e tudo depende de seus gostos pessoais.

Que tal assistir a todo um excelente curso sobre o tema com o prof. Kretcheu? [Acesse aqui](https://www.youtube.com/playlist?list=PLuf64C8sPVT9L452PqdyYCNslctvCMs_n)

Ou ler um bom livro? [clique aqui](https://debian-handbook.info/browse/pt-BR/stable/)

Ou um bate papo mais amplo no grupo da [comunidade depxp](https://t.me/debxpcomunidade) lá no Telegram?
